const express = require('express');
const User = require('../models/user');
const router = express.Router();
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const login = passport.authenticate('local', {
  failureRedirect: '/users/login',
  successRedirect: '/users',
  failureFlash: true,
  successFlash: true
});
passport.use(new LocalStrategy((username, password, done) => {
  User.getUserByUserName(username, (err, user) => {
    if (err) {
      throw new Error(err);
    }
    if (!user) {
      return done(null, false, {message: 'Unknown User'});
    }
    console.log('USER BY UNAME | SUCCESS:', user);
    User.comparePassword(password, user.password, (err, isMatch) => {
      if (err) {
        throw new Error(err);
      }
      if (isMatch) {
        console.log('PASSWORD | DONE:', user);
        return done(null, user, {message: 'Successfully loged in'});
      } else {
        console.log('PASSWORD | INVALID:', user);
        return done(null, false, {message:'Invalid password'});
      }
    });
  });
}));
passport.serializeUser(function(user, done) {
  done(null, user.id);
});
passport.deserializeUser(function(id, done) {
  User.getUserById(id, (err, user) => {
    done(err, user);
  });
});

router.get('/', enshureAuthentication, (req, res, next) => {
  console.log('USERS');
  res.render('users', {});
});
function enshureAuthentication(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('success_msg', 'You are not loged in!');
    res.redirect('/users/login');
  }
}
// -------------------------------------------
router.get('/registration', (req, res, next) => {
  console.log('REGISTRATION');
  res.render('registration', {});
});
router.post('/registration', (req, res, next) => {
  console.log('REGISTRATION:', req.body);
  const name     = req.body.name;
  const username = req.body.username;
  const email    = req.body.email;
  const password = req.body.password;
  const confirm  = req.body.confirm;
  req.checkBody('name',     'name is required').notEmpty();
  req.checkBody('username', 'username is required').notEmpty();
  req.checkBody('email',    'email is required').notEmpty();
  req.checkBody('password', 'password is required').notEmpty();
  req.checkBody('confirm',  'confirm is required').notEmpty();
  const errors = req.validationErrors();
  if (errors && errors.length) {
    res.render('registration', {errors});
  } else {
    console.log('PASSED');
    const newUser = User({ name, username, email, password, confirm, });
    User.createUser(newUser, (err, user) => {
      if (err) throw new Error(err);
      console.log('CREATED USER:', user);
    });
    req.flash('success_msg', 'Successfully registred');
    res.redirect('/users');
  }
});
// -------------------------------------------
router.get('/login', (req, res, next) => {
  console.log('LOGIN');
  res.render('login', {});
});
router.post('/login', login, (req, res) => {});
router.get('/logout', (req, res) => {
  console.log('LOGOUT');
  req.logout();
  req.flash('success_msg', 'You are loged out');
  res.redirect('/users/login');
});

module.exports = router;
