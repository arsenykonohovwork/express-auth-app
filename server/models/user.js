const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');


const UserSchema = mongoose.Schema({
  username: {
    type: String,
    index: true
  },
  password: {
    type: String,
  },
  email: {
    type: String,
  },
  name: {
    type: String,
  },
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
  
module.exports.createUser = function (user, cb) {
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(user.password, salt, (err, hash) => {
      user.password = hash;
      user.save(cb);
    });
  });
};
module.exports.getUserByUserName = function (username, cb) {
  const query = {username};
  User.findOne(query, cb);
};
module.exports.getUserById = function (id, cb) {
  User.findById(id, cb);
};
module.exports.comparePassword = function (candidate, hash, cb) {
  bcrypt.compare(candidate, hash, (err, isMatch) => {
    if (err) throw new Error(err);
    cb(null, isMatch);
  });
};

